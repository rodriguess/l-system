# Projet L-System

## Utilisation

Pour executer le scrip il suffit d'utiliser cette commande.

```sh
py ./LSystem.py
```

Au lancement il vous sera demander d'entrer le chemin du fichier contenant les instruction pour generer le L-System.

Vous pouver aussi directement specifié ce chemin au lancement avec l'argument `-i` suivie du chemin vers le fichier à generer.

```sh
py ./LSystem.py -i <chemin du fichier>
```

Il est aussi possible de specifier le ficier de sorti pour ne pas avoir la liste des instruction dans la console mais dans un fichier qui pourra ensuite etre executer pour desiner le L-Systeme demandé.

```sh
py ./LSystem.py -o <chemin du fichier de sortie>
```

Si vous voulez juste voir le lSystem et ne générer ni fichier de sortie ni d'affiche CLI des commande vous pouvez utiliser l'argument `-n`.

```sh
py ./LSystem.py -i <chemin du fichier> -n
```

Bien sur les argument peuvent etres combiné.

```sh
py ./LSystem.py -i <chemin du fichier> -o <chemin du fichier de sortie>
```

Il est aussi possible de passer l'animation lors de la generation du L-System en utilisant la touche `S`

## Structure des fichier L-System

```
axiome = "Y"
regles =
    "X=X[-FFF][+FFF]FX"
    "Y=YFX[+Y][-Y]"
angle  = 25.7
taille = 6
niveau = 7
```

Le fichier doit comporter au minimum:

- Un `axiome`
- Une ou plusieurs `regles` sur un des caracteres de l'axiome
- Un `angle`
- Une `taille` de base pour la longueur de chaque trait
- Le `niveau` de recursivité

Lorsque certaines instruction son presente dans les regles ou l'axiome certain autre paramettre doivent etre present:

- lorsqu'un `<` ou un `>` est present dans les regles il faut rajouter le paramettre `fact_taille`  qui definie un facteur d'agrendisement du trait.
- Il est possible d'utiliser des L-System Stochastic en rajoutant la probabilité associé a la regle au bout de la ligne
    ```
    regles =
        "X=F[++X]F[-X]+X":0.4
        "X=F[+X]F[-X]+X":0.6
    ```

### Symboles acceptés

- `a`, `F` : Deplacement de `taille` pixels avec tracé
- `b`, `f` : Deplacement de `taille` pixels sans tracé
- `+` : Troune à droite de `angle`°
- `-` : Troune à gauche de `angle`°
- `*`, `|` : Demi-tour
- `[` : Enregistre la position actuel
- `]` : Restaure la dernier position enregistrer
- `>` : Augmente `taille` d'un facteur `fact_taille`
- `<` : Diminue `taille` d'un facteur `fact_taille`

Tout autre symbole utilisé ne sera pas interprété. Néamoins ils peuvent servir à créer des règles supplementaires.

### Exemple

`fact_taille`
```
axiome = "c"
regles =
    "F=>F<"
    "c=F[+x]Fd"
    "d=F[-y]Fc"
    "x=c"
    "y=d"
angle  = 45
taille = 4
niveau = 12
fact_taille = 1.36
```
