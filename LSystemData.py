from sys import stderr
from time import strftime, localtime


def erreur(str, critique=False):
    """Ecrit dans la sortie standart d'erreur et quite le programme si demander"""
    time = strftime("%d/%m/%Y, %H:%M:%S", localtime())
    print(time + " | ERREUR: " + str, file=stderr)
    if critique:
        exit()


liste_fn_base = ["a", "F", "b", "f", "+", "-", "*", "|", "[", "]", ">", "<"]


class LSystemData:
    """
    Classe permettant de recuperer les parametres d'un lSystem depuis un fichier
    """

    data = {"regles": {}}
    """Variable contenant les données permettant de construir un lSystem"""

    __lignes_fichier = []
    """Liste contenant chaque ligne d'un fichier"""
    def __lis_axiome(self, nbline):
        """Lis l'axiome dans le fichier et le stock en memoire

        @param self: Classe parente de la methode
        @param nbline: Ligne à laquel l'axiome ce trouve
        """
        self.data["axiome"] = self.__lignes_fichier[
            nbline][self.__lignes_fichier[nbline].index('\"') +
                    1:self.__lignes_fichier[nbline].rindex('\"')]

    def __lis_regle(self, nbline):
        """Lis les règles dans le fichier et les stock en memoire

        @param self: Classe parente de la methode
        @param nbline: Ligne ou la liste de règle commence
        """
        regle = []
        i = 1
        while (nbline + i) < len(
                self.__lignes_fichier) and self.__lignes_fichier[
                    nbline + i].strip()[0] == '\"':
            proba = float(1.0)
            regle = self.__lignes_fichier[
                nbline + i][self.__lignes_fichier[nbline + i].index('\"') +
                            1:self.__lignes_fichier[nbline + i].rindex('\"')]
            if (j := self.__lignes_fichier[nbline + i].find(":")) >= 0:
                proba = float(self.__lignes_fichier[nbline + i][j + 1:])
            if regle[0] not in self.data["regles"]:
                self.data["regles"][regle[0]] = [[], []]
            self.data["regles"][regle[0]][0].append(regle[2:])
            self.data["regles"][regle[0]][1].append(proba)
            i += 1

    def __lis_angle(self, nbline):
        """Lis l'angle dans le fichier et le stock en memoire

        @param self: Classe parente de la methode
        @param nbline: Ligne à laquel l'angle ce trouve
        """
        self.data["angle"] = float(self.__lignes_fichier[nbline]
                                   [self.__lignes_fichier[nbline].index('=') +
                                    1:])

    def __lis_taille(self, nbline):
        """Lis la taille dans le fichier et la stock en memoire

        @param self: Classe parente de la methode
        @param nbline: Ligne à laquel la taille ce trouve
        """
        self.data["taille"] = float(self.__lignes_fichier[nbline]
                                    [self.__lignes_fichier[nbline].index('=') +
                                     2:])

    def __lis_niveau(self, nbline):
        """Lis le niveau dans le fichier et le stock en memoire

        @param self: Classe parente de la methode
        @param nbline: Ligne à laquel le niveau ce trouve
        """
        self.data["niveau"] = int(self.__lignes_fichier[nbline]
                                  [self.__lignes_fichier[nbline].index('=') +
                                   2:])

    def __lis_fact_taille(self, nbline):
        """Lis le niveau dans le fichier et le stock en memoire

        @param self: Classe parente de la methode
        @param nbline: Ligne à laquel le facteur de taille ce trouve
        """
        self.data["fact_taille"] = float(
            self.__lignes_fichier[nbline]
            [self.__lignes_fichier[nbline].index('=') + 2:])

    __fn_traitement = {
        "axiome": __lis_axiome,
        "regles": __lis_regle,
        "angle": __lis_angle,
        "taille": __lis_taille,
        "niveau": __lis_niveau,
        "fact_taille": __lis_fact_taille
    }
    """Dictionnaire permettant d'acceder aux methodes correpondant aux lignes lues"""

    def __test_data(self):
        """Test si les données lues sont correctes

        @param self: Classe parente de la methode
        """
        for key_test in ["axiome", "regles", "angle", "taille", "niveau"]:
            if key_test not in self.data:
                erreur(str(key_test) + " non trouvé ou incorrect !!", True)
        for char in self.data["axiome"]:
            if str(char) not in self.data["regles"] and str(
                    char) not in liste_fn_base:
                erreur(
                    "Aucune regle trouvé pour \"" + str(char) +
                    "\" dans l'axiome !", True)
        for rule in self.data["regles"]:
            for probas in self.data["regles"][rule][0]:
                for char in probas[0]:
                    if str(char) not in probas[0] and str(
                            char) not in liste_fn_base:
                        erreur(
                            "Aucune regle trouvé pour \"" + str(char) +
                            "\" dans la liste de regles !", True)

    def __init__(self, chemin):
        """Initialise la classe

        @param self: Classe parente de la methode
        @param chemin: Chemin vers le fichier a lire
        """
        fichier = open(chemin, "r")
        self.__lignes_fichier = fichier.readlines()
        for number, line in enumerate(self.__lignes_fichier):
            if line.split(' ')[0] in self.__fn_traitement:
                self.__fn_traitement[line.split(' ')[0]](self, number)
        self.__test_data()


def main():
    lsystem = LSystemData("./axiome.txt")
    print(lsystem.data)


if __name__ == "__main__":
    main()
