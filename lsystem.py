from sys import argv
from LSystemData import *
import tkinter as tk
import turtle as tr
from bisect import bisect
from random import random
from queue import LifoQueue
canvsize = 1000
wn_height = 720
wn_width = 1280

root = tk.Tk()
root.geometry(str(wn_width) + "x" + str(wn_height))

frame = tk.Frame(root)
frame.pack(expand=1, fill=tk.BOTH)

canvas = tr.ScrolledCanvas(frame,
                           canvwidth=2 * wn_width,
                           canvheight=2 * wn_height)
canvas.pack(expand=1, fill=tk.BOTH)

screen = tr.TurtleScreen(canvas)
#screen.bgcolor("#181A1B")
tr = tr.RawTurtle(screen, visible=True)
tr.speed(0)
#tr.color("#FFFFFF")

buff_pos = LifoQueue()


def output(lsys_data, str):
    """Gère les sorties vers un fichier ou stdout

    Si la valeur de l'entré out_type du dictionnaire lsys_data est à 1 alors écrit dans stdout.
    Si elle est a 2 écrit dans un fichier defini par l'entré out_fil de lsys_data sinon ne fait rien


    @param lsys_data: Dictionnaire contenant toutes les données necessaire a l'execution du lSystem
    @param str: chaine de caractère à écrire
    """
    if lsys_data["out_type"] == 1 and str:
        print(str)
    elif lsys_data["out_type"] == 2 and str:
        lsys_data["out_file"].write(str)


def deplace_avec_trace(lsys_data):
    """Deplace le curseur en laissant une trace

    @param lsys_data: Dictionnaire contenant toutes les données necessaire a l'execution du lSystem
    @return: La chaine de caractere correspondant au instruction turtle utilisé par la fonction
    """
    tr.pd()
    tr.fd(lsys_data["taille"])
    return ("pd();fd(" + str(lsys_data["taille"]) + ");\n")


def deplace_sans_trace(lsys_data):
    """Deplace le curseur sans laisser de trace

    @param lsys_data: Dictionnaire contenant toutes les données necessaire a l'execution du lSystem
    @return: La chaine de caractere correspondant au instruction turtle utilisé par la fonction
    """
    tr.pu()
    tr.fd(lsys_data["taille"])
    return ("pu();fd(" + str(lsys_data["taille"]) + ");\n")


def tourne_droite(lsys_data):
    """Change la direction du curseur d'un certain angle dans le sense des aiguilles d'une montre

    @param lsys_data: Dictionnaire contenant toutes les données necessaire a l'execution du lSystem
    @return: La chaine de caractere correspondant au instruction turtle utilisé par la fonction
    """
    tr.right(lsys_data["angle"])
    return ("right(" + str(lsys_data["angle"]) + ");\n")


def tourne_gauche(lsys_data):
    """Change la direction du curseur d'un certain angle dans le sense inversse des aiguilles d'une montre

    @param lsys_data: Dictionnaire contenant toutes les données necessaire a l'execution du lSystem
    @return: La chaine de caractere correspondant au instruction turtle utilisé par la fonction
    """
    tr.left(lsys_data["angle"])
    return ("left(" + str(lsys_data["angle"]) + ");\n")


def sauve_pos(lsys_data):
    """Sauvegarde la position du curseur dans un liste de type Last in First Out

    @param lsys_data: Dictionnaire contenant toutes les données necessaire a l'execution du lSystem
    """
    global buff_pos
    buff_pos.put([tr.pos(), tr.heading()])
    #[(x,y),angle]


def rest_pos(lsys_data):
    """Restore la position du curseur

    @param lsys_data: Dictionnaire contenant toutes les données necessaire a l'execution du lSystem
    @return: La chaine de caractere correspondant au instruction turtle utilisé par la fonction
    """
    pos = buff_pos.get()
    tr.setpos(pos[0])
    tr.setheading(pos[1])
    return ("setpos(" + str(pos[0]) + ");setheading(" + str(pos[1]) + ");\n")


def demi_tour(lsys_data):
    """Fait un demi tour

    @param lsys_data: Dictionnaire contenant toutes les données necessaire a l'execution du lSystem
    @return: La chaine de caractere correspondant au instruction turtle utilisé par la fonction
    """
    tr.right(180)
    return ("right(180);\n")


def augmente_taille(lsys_data):
    """Augmente la longeur des trait d'un certain facteur

    @param lsys_data: Dictionnaire contenant toutes les données necessaire a l'execution du lSystem
    """
    lsys_data["taille"] *= lsys_data["fact_taille"]


def diminue_taille(lsys_data):
    """Reduit la longeur des trait d'un certain facteur

    @param lsys_data: Dictionnaire contenant toutes les données necessaire a l'execution du lSystem
    """
    lsys_data["taille"] /= lsys_data["fact_taille"]


fn_deplacement_Lsys = {
    "a": deplace_avec_trace,
    "F": deplace_avec_trace,
    "b": deplace_sans_trace,
    "f": deplace_sans_trace,
    "+": tourne_droite,
    "-": tourne_gauche,
    "*": demi_tour,
    "|": demi_tour,
    "[": sauve_pos,
    "]": rest_pos,
    ">": augmente_taille,
    "<": diminue_taille
}
"""Dictionnaire permettant d'acceder aux fonction correpondant aux instructions entrées"""


def choisie_regle(lsys_data, char):
    """Permet de determiner quel regle utiliser en fonction des probabilités qui y sont associé

    Lis la règle associé au caractère passé en paramètre. si plusieurs règle existe pour un même
    caractère lis la probabilité associé et choisie une des regles associé au hassard en prenent
    compte des probabilité associé.

    @param lsys_data: Dictionnaire contenant toutes les données necessaire a l'execution du lSystem
    @return: Chaine de caractère correspondant a la regle associé au caractère passé en paramètre
    """
    if len(lsys_data["regles"][char][0]) == 1:
        return lsys_data["regles"][char][0][0]
    r = random()
    index = 0
    while (r >= 0 and index < len(lsys_data["regles"][char][1])):
        r -= lsys_data["regles"][char][1][index]
        index += 1
    return lsys_data["regles"][char][0][index - 1]


def gen_instruc(lsys_data):
    """Genere les instruction a executé en fonction du niveau du lSystem specifié

    @param lsys_data: Dictionnaire contenant toutes les données necessaire a l'execution du lSystem
    """
    for i in range(lsys_data["niveau"]):
        new_axiome = ""
        for char in lsys_data["axiome"]:
            if char in lsys_data["regles"]:
                new_axiome += choisie_regle(lsys_data, char)
            else:
                new_axiome += char
        lsys_data["axiome"] = new_axiome


def desactive_animation(a):
    """Desactive l'animation du traçage"""
    screen.tracer(False)


def init_system(path):
    """Initialise le lSystem a partir d'un fichier et retourne un dictionnaire contenant toutes les données necessaire

    @param path: Chemin vers le fichier contenant les parametre du lSystem a créer
    @return: Un dictionnaire contenant les données permettant de construir le lSystem
    """
    lsys = LSystemData(path)
    gen_instruc(lsys.data)
    return lsys.data


def draw_system(lsys_data):
    """Dessine le lSystem dans un fenetre

    @param lsys_data: Dictionnaire contenant toutes les données necessaire a l'execution du lSystem
    """
    for char in lsys_data["axiome"]:
        if char in fn_deplacement_Lsys:
            output(lsys_data, fn_deplacement_Lsys[char](lsys_data))


root.bind("s", desactive_animation)
"""Associé l'appuye de la touche s avec la desactivation de l'animation"""


def main():
    """Fonction principale permettant d'initalisé et d'executer le proggramme en fonction des argument fournis"""
    global canvas
    out = 1
    out_file = None
    path = str()
    for nb, arg in enumerate(argv):
        if arg == "-i":
            path = argv[nb + 1]
        if arg == "-o":
            out = 2
            out_file = open(argv[nb + 1], "w")
            out_file.write(
                "from turtle import *\nspeed('fastest');color('black')\n")
        if arg == "-n":
            out = 0
    if not path:
        path = input("Entrer le chemin du fichier a utiliser: ")
    lsys_data = init_system(path)
    lsys_data["out_type"] = out
    lsys_data["out_file"] = out_file
    draw_system(lsys_data)
    if lsys_data["out_file"]:
        lsys_data["out_file"].write("exitonclick();")
    screen.update()
    print("Fini !")
    screen.mainloop()


if __name__ == "__main__":
    main()
